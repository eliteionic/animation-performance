import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	cards: any = new Array(5);

	constructor(public navCtrl: NavController) {

	}

}
